import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimeiroModuloComponent } from './primeiro-modulo.component';
import {TerceiroModuloModule} from '../terceiro-modulo/terceiro-modulo.module'

@NgModule({
  imports: [
    CommonModule,
    TerceiroModuloModule
  ],
  declarations: [
    PrimeiroModuloComponent
  ],
  exports:[
    PrimeiroModuloComponent
  ]

})
export class PrimeiroModuloModule { }
