import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-primeiro-modulo',
  templateUrl: './primeiro-modulo.component.html',
  styleUrls: ['./primeiro-modulo.component.css']
})
export class PrimeiroModuloComponent implements OnInit {

  nomes: any = [];

  @Output() enviar = new EventEmitter();

  salvar(nome: string ){
    console.log(nome);
    this.nomes.push({nome:nome});
    this.enviar.emit(this.nomes);

  }

  constructor() {
   }

  ngOnInit() {
  }

}
