import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SegundoModuloComponent } from './segundo-modulo.component';

describe('SegundoModuloComponent', () => {
  let component: SegundoModuloComponent;
  let fixture: ComponentFixture<SegundoModuloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SegundoModuloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SegundoModuloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
