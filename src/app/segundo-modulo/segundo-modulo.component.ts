import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-segundo-modulo',
  templateUrl: './segundo-modulo.component.html',
  styleUrls: ['./segundo-modulo.component.css']
})
export class SegundoModuloComponent implements OnInit {

  @Input() nomes = [];

  constructor() {
  }

  ngOnInit() {
  }

}
