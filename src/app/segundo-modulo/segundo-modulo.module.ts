import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SegundoModuloComponent } from './segundo-modulo.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SegundoModuloComponent
  ],
  exports:[
    SegundoModuloComponent
  ]
})
export class SegundoModuloModule { }
