import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TerceiroModuloComponent } from './terceiro-modulo.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TerceiroModuloComponent
  ],
  exports:[
    TerceiroModuloComponent
  ]
})
export class TerceiroModuloModule { }
