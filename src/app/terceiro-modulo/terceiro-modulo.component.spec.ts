import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerceiroModuloComponent } from './terceiro-modulo.component';

describe('TerceiroModuloComponent', () => {
  let component: TerceiroModuloComponent;
  let fixture: ComponentFixture<TerceiroModuloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerceiroModuloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerceiroModuloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
