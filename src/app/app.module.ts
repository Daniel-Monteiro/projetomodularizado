import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {PrimeiroModuloModule} from './primeiro-modulo/primeiro-modulo.module';
import {SegundoModuloModule} from './segundo-modulo/segundo-modulo.module'
import {TerceiroModuloModule} from './terceiro-modulo/terceiro-modulo.module'

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PrimeiroModuloModule,
    SegundoModuloModule,
    TerceiroModuloModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
